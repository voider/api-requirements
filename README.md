product list, product details, register product
login, user details, register user
purchase details, insert/read purchase


<h2>Product services</h2>
GET  <b>/store/product?perPage=2&pageNum=2&id=3</b>
query params: perPage:int, pageNum:int, searchFilters - id, name, category

POST <b>/store/product</b>
req: {productId:int, name:string, description:string, categoryId:[int], quantity:int}

PUT  <b>/store/product/12345</b>
req: {productId:int, name:string, description:string, categoryId:[int], quantity:int}

DELETE <b>/store/product/12345</b>

<h2>Category services</h2>
GET <b>/store/category</b>
query params: name:string

POST <b>/store/category</b>
req: {name:string, description:string}

DELETE <b>/store/category/123</b>

<h2>User services</h2>
GET <b>/store/user</b>
query params: perPage:int, pageNum:int, searchFilters - id, name, role

POST <b>/store/user</b>
req:{name:string, email:string, password:string, address:string, roleId:int}, 

PUT <b>/store/user/1234</b>
req: {name:string, email:string, adress:string, roleId:int}

DELETE <b>/store/user/1234</b>

<h2>Login</h2>
POST <b>/store/auth</b>
req: {email:string, password:string}
response status codes: 403, 200

<h2>Purchase</h2>
GET <b>/store/purchase</b>
query params: username:string, dateFrom:date, dateTo:string, categoryId:string

POST <b>/store/purchase</b>
req:{purchaseDate:date, username:string, productId:int, quantity:int}
